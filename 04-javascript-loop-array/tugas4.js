// soal 1 ( Membuat Looping sederhana )
for(let i = 0; i < 10; i++){
    console.log(i);
}

// // soal 2 ( Membuat Looping dengan conditional angka ganjil )
for(let a = 0; a < 10; a++){
    if ( a %  2 == 1){
        console.log(a);
    }
}


// // soal 3 ( Membuat Looping dengan conditional angka genap )
for(let b = 0; b < 10; b++){
    if ( b %  2 == 0){
        console.log(b);
    }
}

// // soal 4 ( Mengakses element array )
let array1 = [1,2,3,4,5,6]
console.log(array1[5]);

// // soal 5 ( Mengurutkan element array )
let array2 = [5,2,4,1,3,5]
console.log(array2.sort());

// // Soal 6 ( Mengeluarkan element array )
let array3 = ["selamat", "anda", "melakukan", "perulangan", "array", "dengan", "for"]
for(let hasilArray3 = 0; hasilArray3<array3.length; hasilArray3++){
    console.log(array3[hasilArray3]);
}

// // Soal 7 ( Mengeluarkan element array dan dengan kondisi )
let array4 = [1, 2, 3, 4, 5, 6,7, 8, 9, 10];
for(let i = 0; i < array4.length; i++){
    if(array4[i] % 2 == 0){
        console.log(array4[i]);
    }
}
// // Soal 8 ( menggabungkan element menjadi string )
let kalimat= ["saya", "sangat", "senang", "belajar", "javascript"];
let hasilKalimat=kalimat.join(" ");
console.log(hasilKalimat);

// // soal 9 ( Menambahkan element array )
var sayuran=[];
sayuran.push("Kangkung","Bayam","Buncis","Kubis","Timun","Seledri","Tauge");
console.log(sayuran);
