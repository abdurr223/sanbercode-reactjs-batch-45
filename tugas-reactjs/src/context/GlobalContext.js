import React, { createContext, useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

export const GlobalContext = createContext();

export const GlobalProvider = (props) => {
  let navigate = useNavigate();

  // fetching data
  const [data, setData] = useState(null);

  // create data
  const [input, setInput] = useState({
    name: "",
    course: "",
    score: 0,
  });

  //indikator
  const [fetchStatus, setFetchStatus] = useState(true);
  //indikator
  const [currentId, setCurrentId] = useState(-1);

  useEffect(() => {
    if (fetchStatus === true) {
      axios
        .get("https://backendexample.sanbercloud.com/api/student-scores")
        .then((res) => {
          setData([...res.data]);
        })
        .catch((error) => {});
      setFetchStatus(false);
    }
  }, [fetchStatus, setFetchStatus]);

  const handleInput = (event) => {
    let name = event.target.name;
    let value = event.target.value;

    setInput({ ...input, [name]: value });
  };

  //handling submit
  const handleSubmit = (event) => {
    event.preventDefault();

    let { name, course, score } = input;

    if (currentId === -1) {
      //create data
      axios
        .post("https://backendexample.sanbercloud.com/api/student-scores", {
          name,
          course,
          score,
        })
        .then((res) => {
          console.log(res);
          setFetchStatus(true);
          navigate("/tugas13");
        });
    } else {
      // update data
      axios
        .put(
          `https://backendexample.sanbercloud.com/api/student-scores/${currentId}`,
          { name, course, score }
        )
        .then((res) => {
          setFetchStatus(true);
          navigate("/tugas13");
        });
    }

    //balikin indikator ke -1
    setCurrentId(-1);

    //clear input setelah create data
    setInput({
      name: "",
      course: "",
      score: 0,
    });
  };

  const handleDelete = (event) => {
    let idData = parseInt(event.target.value);

    axios
      .delete(
        `https://backendexample.sanbercloud.com/api/student-scores/${idData}`
      )
      .then((res) => {
        setFetchStatus(true);
      });
  };

  const handleEdit = (event) => {
    let idData = parseInt(event.target.value);

    setCurrentId(idData);
    navigate(`/edit/${idData}`);
  };

  const handleIndexScore = (nilai) => {
    if (nilai >= 80) {
      return "A";
    } else if (nilai >= 70 && nilai < 80) {
      return "B";
    } else if (nilai >= 60 && nilai < 70) {
      return "C";
    } else if (nilai >= 50 && nilai < 60) {
      return "D";
    } else {
      return "E";
    }
  };

  let state = {
    data,
    setData,
    input,
    setInput,
    fetchStatus,
    setFetchStatus,
    currentId,
    setCurrentId,
  };

  let handleFunction = {
    handleInput,
    handleSubmit,
    handleDelete,
    handleEdit,
    handleIndexScore,
  };

  return (
    <GlobalContext.Provider
      value={{
        state,
        handleFunction,
      }}
    >
      {props.children}
    </GlobalContext.Provider>
  );
};
