import React from "react";
import { Navbar } from "flowbite-react";
import { Link } from "react-router-dom";

const navbar = () => {
  return (
    <>
      <Navbar fluid={true} rounded={true} className="w-3/4 m-auto">
        <Navbar.Brand href="https://flowbite.com/">
          <span className="self-center whitespace-nowrap text-xl font-semibold dark:text-white">
            Kelas Reactjs
          </span>
        </Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse>
            <Link className="md:text-blue-700" to="/">Home</Link>
            <Link to="/tugas13">Tugas13</Link>
        </Navbar.Collapse>
      </Navbar>
    </>
  );
};

export default navbar;
