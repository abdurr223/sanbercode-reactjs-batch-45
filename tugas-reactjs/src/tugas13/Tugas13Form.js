import React, { useContext, useEffect } from "react";
import { GlobalContext } from "../context/GlobalContext";
import { useParams } from "react-router-dom";
import axios from "axios";

const Tugas13Form = () => {
  let { IdData } = useParams();

  const { state, handleFunction } = useContext(GlobalContext);

  const {
    data,
    setData,
    input,
    setInput,
    fetchStatus,
    setFetchStatus,
    currentId,
    setCurrentId,
  } = state;

  const {
    handleInput,
    handleSubmit,
    handleDelete,
    handleEdit,
    handleIndexScore,
  } = handleFunction;

  useEffect(() => {
    if (IdData !== undefined) {
      axios
        .get(
          `https://backendexample.sanbercloud.com/api/student-scores/${IdData}`
        )
        .then((res) => {
          let data = res.data;

          setInput({
            name: data.name,
            course: data.course,
            score: data.score,
          });
        });
    } else {
        setInput({
            name: "",
            course: "",
            score: 0,
          });
    }
  }, []);

  return (
    <>
      <div className="overflow-x-auto relative  sm:rounded-lg w-3/4 m-auto min-w-min my-10">
        <form onSubmit={handleSubmit}>
          <div className="mb-6">
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
              Name :
            </label>
            <input
              type="text"
              onChange={handleInput}
              value={input.name}
              name="name"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="nama..."
              required
            />
          </div>
          <div className="mb-6">
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
              Mata Kuliah :
            </label>
            <input
              type="text"
              onChange={handleInput}
              value={input.course}
              name="course"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="mata kuliah..."
              required
            />
          </div>
          <div className="mb-6">
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
              Nilai :
            </label>
            <input
              type="number"
              onChange={handleInput}
              value={input.score}
              name="score"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="0"
              required
            />
          </div>
          <input
            type="submit"
            className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
          />
        </form>
      </div>
    </>
  );
};

export default Tugas13Form;
