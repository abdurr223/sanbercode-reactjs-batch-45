import React, { useContext } from "react";
import { GlobalContext } from "../context/GlobalContext";
import { Link } from "react-router-dom";

const Tugas13List = () => {
  const { state, handleFunction } = useContext(GlobalContext);

  const {
    data,setData,
    input,setInput,
    fetchStatus,setFetchStatus,
    currentId,setCurrentId,
  } = state;

  const {
    handleInput,
    handleSubmit,
    handleDelete,
    handleEdit,
    handleIndexScore,
  } = handleFunction;

  return (
    <>
      <div className="w-3/4 m-auto min-w-min mt-10">
        <Link to='/create'>
        <button
          type="button"
          className="w-auto text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800"
        >
          Create Data
        </button>
        </Link>
      </div>
      <div className="overflow-x-auto relative shadow-md sm:rounded-lg w-3/4 m-auto min-w-min">
        <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
          <thead className="bg-purple-600 text-white text-xs uppercase rounded-t-lg">
            <tr>
              <th scope="col" className="py-3 px-6">NO</th>
              <th scope="col" className="py-3 px-6">NAMA</th>
              <th scope="col" className="py-3 px-6">MATA KULIAH</th>
              <th scope="col" className="py-3 px-6">NILAI</th>
              <th scope="col" className="py-3 px-6">INDEX NILAI</th>
              <th scope="col" className="py-3 px-6">ACTION</th>
            </tr>
          </thead>
          <tbody>
            {data !== null &&
              data.map((res, index) => {
                return (
                  <tr
                    className="bg-white border-b dark:bg-gray-800 dark:border-gray-700"
                    key={index}
                  >
                    <th
                      scope="row"
                      className="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white"
                    >
                      {index + 1}
                    </th>
                    <td className="py-4 px-6">{res.name}</td>
                    <td className="py-4 px-6">{res.course}</td>
                    <td className="py-4 px-6">{res.score}</td>
                    <td className="py-4 px-6">{handleIndexScore(res.score)}</td>
                    <td className="py-4 px-6">
                      <div className="flex text-white">
                        <div>
                          <button
                            onClick={handleEdit}
                            className="w-auto bg-amber-400 rounded-lg"
                            value={res.id}
                          >
                            Edit
                          </button>
                        </div>
                        <div>
                          <button
                            onClick={handleDelete}
                            className="w-auto bg-red-500 ml-5 rounded-lg"
                            value={res.id}
                          >
                            Delete
                          </button>
                        </div>
                      </div>
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default Tugas13List;
