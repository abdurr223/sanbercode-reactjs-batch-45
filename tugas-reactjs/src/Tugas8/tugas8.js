import React, {useState}from 'react';
import '.././App.css';

function App() {

  let [count, setCount] = useState(0);

  const handleCount = () => {
    setCount(count += 1);
  };

  return (
    <>
      <div className="card">
        <h1>{count}</h1>
        <button onClick={handleCount}>Tambah</button>
        {count >= 10 ? <p>State count sudah lebih dari 10!!</p> : false}
      </div>
    </>
  );
}

export default App;