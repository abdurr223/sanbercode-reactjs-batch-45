import React from "react";
import { GlobalProvider } from "./context/GlobalContext";
// import Tugas10 from "./tugas10/Tugas10";
// import Tugas11 from "./tugas11/Tugas11";
// import Tugas12 from "./tugas12/Tugas12";
// import Tugas13 from "./tugas13/Tugas13";
import Tugas6 from "./tugas6/Tugas6";
// import Tugas7 from "./tugas7/Tugas7";
// import Tugas8 from "./tugas8/Tugas8";
// import Tugas9 from "./tugas9/Tugas9";
import Tugas13List from "./tugas13/Tugas13List";
import Tugas13Form from "./tugas13/Tugas13Form";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Navbar from "./components/navbar"

function App() {
  return (
    <>
      <BrowserRouter>
        <GlobalProvider>
                   <Navbar />

          <Routes>
            <Route path="/" element={<Tugas6 />} />
            <Route path="/tugas13" element={<Tugas13List />} />
            <Route path="/create" element={<Tugas13Form />} />
            <Route path="/edit/:IdData" element={<Tugas13Form />} />
          </Routes>


        </GlobalProvider>
      </BrowserRouter>
    </>
  );
}

export default App;
