import React, { useContext, useState, useEffect } from "react";
import { GlobalContext } from "../context/GlobalContext";

const Tugas12 = () => {
  
  const {state, handleFunction} = useContext(GlobalContext)

  const {
    data, setData,
    input, setInput,
    fetchStatus, setFetchStatus,
    currentId, setCurrentId 
  } = state 

  const {
    handleInput,
    handleSubmit,
    handleDelete,
    handleEdit,
    handleIndexScore
  } = handleFunction

  return (
    <>
      <div className="overflow-x-auto relative shadow-md sm:rounded-lg w-3/4 m-auto min-w-min my-10">
        <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
          <thead className="bg-purple-600 text-white text-xs uppercase">
            <tr>
              <th scope="col" className="py-3 px-6">NO</th>
              <th scope="col" className="py-3 px-6">NAMA</th>
              <th scope="col" className="py-3 px-6">MATA KULIAH</th>
              <th scope="col" className="py-3 px-6">NILAI</th>
              <th scope="col" className="py-3 px-6">INDEX NILAI</th>
              <th scope="col" className="py-3 px-6">ACTION</th>
            </tr>
          </thead>
          <tbody>
            {data !== null &&
            data.map((res, index) => {
                return (
                  <tr
                    className="bg-white border-b dark:bg-gray-800 dark:border-gray-700"
                    key={index}
                  >
                    <th
                      scope="row"
                      className="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white"
                    >
                      {index + 1}
                    </th>
                    <td className="py-4 px-6">{res.name}</td>
                    <td className="py-4 px-6">{res.course}</td>
                    <td className="py-4 px-6">{res.score}</td>
                    <td className="py-4 px-6">{handleIndexScore(res.score)}</td>
                    <td className="py-4 px-6">
                      <div className="flex text-white">
                        <div>
                          <button
                            onClick={handleEdit}
                            className="w-auto bg-amber-400 rounded-lg"
                            value={res.id}
                          >
                            Edit
                          </button>
                        </div>
                        <div>
                          <button
                            onClick={handleDelete}
                            className="w-auto bg-red-500 ml-5 rounded-lg"
                            value={res.id}
                          >
                            Delete
                          </button>
                        </div>
                      </div>
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </table>
      </div>

      <div className="overflow-x-auto relative  sm:rounded-lg w-3/4 m-auto min-w-min my-10">
        <form onSubmit={handleSubmit}>
          <div className="mb-6">
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Name :</label>
            <input
              type="text"
              onChange={handleInput}
              value={input.name}
              name="name"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="nama..."
              required
            />
          </div>
          <div className="mb-6">
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Mata Kuliah :</label>
            <input
              type="text"
              onChange={handleInput}
              value={input.course}
              name="course"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="mata kuliah..."
              required
            />
          </div>
          <div className="mb-6">
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Nilai :</label>
            <input
              type="number"
              onChange={handleInput}
              value={input.score}
              name="score"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="0"
              required
            />
          </div>
          <input
            type="submit"
            className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
          />
        </form>
      </div>
    </>
  );
};

export default Tugas12;
