import React from "react";
import "./styles-tugas9.css";
import { useState } from "react";

const Tugas8 = () => {
  let [count, setCount] = useState(0);

  const handleCount = () => {
    setCount(count += 1);
  };

  return (
    <>
      <div className="card">
        <p>{count}</p>
        <button onClick={handleCount}>Tambah</button>
        {count >= 10 ? <p>State count sudah lebih dari 10!!</p> : false}
      </div>
    </>
  );
};

export default Tugas8;
