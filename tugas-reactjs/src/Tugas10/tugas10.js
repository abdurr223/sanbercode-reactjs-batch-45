import React, { useState } from "react";
import { useEffect } from "react";
import axios from "axios";
import '.././App.css';


const Tugas10 = () => {
  const [data, setData] = useState(null);

  useEffect(() => {
    
    const fetchData = async () => {
    axios
      .get("https://backendexample.sanbercloud.com/api/student-scores")
      .then((res) => {
        setData([...res.data]);
      })
      .catch((error) => {});
    }

    fetchData()
  }, []);

  const handleIndexScore = (nilai) => {
    if (nilai >= 80) {
      return "A";
    } else if (nilai >= 70 && nilai < 80) {
      return "B";
    } else if (nilai >= 60 && nilai < 70) {
      return "C";
    } else if (nilai >= 50 && nilai < 60) {
      return "D";
    } else {
      return "E";
    }
  };

  return (
    <>
    <div className="card">
      <div className="overflow-x-auto relative shadow-md sm:rounded-lg w-3/4 m-auto min-w-min my-10">
        <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
          <thead className="bg-purple-600 text-white text-xs uppercase">
            <tr>
              <th scope="col" className="py-3 px-6">NO</th>
              <th scope="col" className="py-3 px-6">NAMA</th>
              <th scope="col" className="py-3 px-6">MATA KULIAH</th>
              <th scope="col" className="py-3 px-6">NILAI</th>
              <th scope="col" className="py-3 px-6">INDEX NILAI</th>
            </tr>
          </thead>
          <tbody>
            {data !== null &&
              data.map((res, index) => {
                return (
                  <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700" key={index}>
                    <th
                      scope="row"
                      className="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white"
                    >
                      {index + 1}
                    </th>
                    <td className="py-4 px-6">{res.name}</td>
                    <td className="py-4 px-6">{res.course}</td>
                    <td className="py-4 px-6">{res.score}</td>
                    <td className="py-4 px-6">{handleIndexScore(res.score)}</td>
                  </tr>
                );
              })}
          </tbody>
        </table>
      </div>
      </div>
    </>
  );
};

export default Tugas10;
