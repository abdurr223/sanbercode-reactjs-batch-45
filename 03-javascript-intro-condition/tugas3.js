// soal 1 ( Latihan Membuat variable dan valuenya )

let namalengkap = "abdurrahman";
console.log(namalengkap);

// soal 2 ( Latihan Penggabungan variable )
let word = 'JavaScript'; 
let second = 'is'; 
let third = 'awesome'; 

let outputGabunganVariable = word + " " + second + " " + third;
console.log(outputGabunganVariable)

// soal 3 ( Latihan penggabungan variable Menggunakan Template Literals)
let hello = 'Hello '; 
let world = 'World!!'; 

let output = `${hello}${world}`;
console.log(output)


// soal 4 ( Latihan Mengubah tipe data )

let panjangPersegiPanjang = "8";
let lebarPersegiPanjang = "5";

let kelilingPersegiPanjang = 2 * ( parseInt(panjangPersegiPanjang) + parseInt(lebarPersegiPanjang));

console.log(kelilingPersegiPanjang);

// soal 5 ( Latihan Mengurai Kalimat )

let sentences= 'wah javascript itu keren sekali'; 

let firstWords= sentences.substring(0, 3); 
let secondWords = sentences.substring(4, 14); 
let thirdWords = sentences.substring(15, 18);  
let fourthWords = sentences.substring(19, 24);  
let fifthWords = sentences.substring(25, 31);  

console.log('Kata Pertama: ' + firstWords); 
console.log('Kata Kedua: ' + secondWords); 
console.log('Kata Ketiga: ' + thirdWords); 
console.log('Kata Keempat: ' + fourthWords); 
console.log('Kata Kelima: ' + fifthWords);

// soal 6 (Akses karakter dalam string),
var sentence = "I am going to be React JS Developer"; 

var exampleFirstWord = sentence[0] ; 
var exampleSecondWord = sentence[2] + sentence[3]  ; 
var thirdWord = sentence[4] + sentence[5]  + sentence[6] + sentence[7] + sentence[8] + sentence[9]; // lakukan sendiri, wajib mengikuti seperti contoh diatas 
var fourthWord = sentence[10] + sentence[11] + sentence[12] + sentence[13];  // lakukan sendiri , wajib mengikuti seperti contoh diatas
var fifthWord = sentence[14] + sentence[15] + sentence[16];// lakukan sendiri , wajib mengikuti seperti contoh diatas
var sixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21] + sentence[22]; // lakukan sendiri , wajib mengikuti seperti contoh diatas
var seventhWord =  sentence[23] + sentence[24] + sentence[25]; // lakukan sendiri , wajib mengikuti seperti contoh diatas
var eighthWord = sentence[26] + sentence[27] + sentence[28] + sentence[29] + sentence[30] + sentence[31] + sentence[32] +  sentence[33] + sentence[34]; // lakukan sendiri , wajib mengikuti seperti contoh diatas

console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + exampleSecondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord);



// soal 7 ( Latihan Mengambil sebuah Kalimat )

// Gunakan metode slice() untuk mengembalikan kata "bananas".

let txt = "I can eat bananas all day";
let hasil = txt.slice(10, 16); //lakukan pengambilan kalimat di variable ini

console.log(hasil)


// soal 8 ( membuat kondisi sederhana )

// buatlah variabel seperti di bawah ini

var nilaiDoeDoe = 50;

// tentukan indeks nilai dari nilaiDoeDoe (tampilkan dengan console.log) dengan kondisi :

// nilai >= 80 indeksnya A
// nilai >= 70 dan nilai < 80 indeksnya B
// nilai >= 60 dan nilai < 70 indeksnya c
// nilai >= 50 dan nilai < 60 indeksnya D
// nilai < 50 indeksnya E

if (nilaiDoeDoe >= 80) {
    console.log("indexnya A");
} else if (nilaiDoeDoe >= 70 && nilaiDoeDoe < 80) {
    console.log("indexnya B");
} else if (nilaiDoeDoe >= 60 && nilaiDoeDoe < 70) {
    console.log("indexnya C");
} else if (nilaiDoeDoe >= 50 && nilaiDoeDoe < 60) {
    console.log("indexnya D");
} else {
    console.log("indexnya E");
}


// soal 9 ( conditional tipe tenary operator )

// Terdapat sebuah kondisional seperti dibawah ini :

let angka = 2

// if(angka === 2){
//   console.log("angka nya 2")
// }else{
//   console.log("bukan angka 2")
// }

let pesan = angka == 2 ? "angka nya 2" : "bukan angka 2";
console.log(pesan);

// soal 10 ( conditional tipe Switch Case )

// buatlah variabel seperti di bawah ini

var traffic_lights = "red";
// Gunakanlah variable diatas sebagai input switch dan pastikan ada 3 case :

// case dengan nama "red" dan mencetak cetak string menggunakan console "berhenti"
// case dengan nama "yellow" dan mencetak cetak string menggunakan console "hati-hati"
// case dengan nama "green" dan mencetak cetak string menggunakan console "berjalan"
// output :

// "berhenti"
switch (traffic_lights){
    case "red": {console.log("berhenti"); break;}
    case "yellow":{console.log("hati-hati"); break;}
    case "green": {console.log("berjalan"); break;}
    default: {console.log('berhenti');}}